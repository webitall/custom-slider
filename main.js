let images = document.querySelectorAll('.slider-item img');
let current = 0;
let bntRight = document.querySelector('.btn-right');
let bntLeft = document.querySelector('.btn-left');
let sliderItem = document.querySelector('.slider-item');
let sliderBtn = document.querySelector('.slider-buttons');
let dots = document.getElementsByClassName('slider-dot');
slider();

function currentSlide(n) {
    slider(current = n);
};

function slider(n) {


    for (var i = 0; i < images.length; i++) {
        images[i].classList.add('opacity0');
    }


    if (current == images.length) {
        
        current = 0;
    } else if (current - 1 == -1) {
        current = images.length - 1;
        current - 1;
    }

    if (n < 1) {
        current = 0;
    }
    
    if (n - 1 == -1) {
        n = images.length - 1;
    }
    images[current].classList.remove('opacity0');
    current++;

    for (var i = 0; i < images.length; i++) {
        dots[i].classList.add('active');
    }
    dots[current - 1].classList.remove('active');

}

function btnLeftFunc() {

    if (current == 1) {
        current = images.length;
        console.log(current);
    } else {
        current--;
    }
    current--;
    slider(current);
    console.log(current);
}

function btnRightFunc() {
    if (current == images.length) {
        current = 0;
    } else {
        current + 1;
    }

    slider(current);
}
setInterval(slider, 2000);
window.addEventListener('resize', function () {
    if (window.innerWidth < 1000) {
        sliderItem.style.height = window.innerWidth - 180 + 'px';
        sliderItem.style.width = window.innerWidth + 'px';
    } else if (window.innerWidth < 500) {
        sliderItem.style.height = window.innerWidth - 100 + 'px';
        sliderBtn.style.marginTop = 20 + 'px';
    }
});
bntLeft.addEventListener('click', btnLeftFunc);
bntRight.addEventListener('click', btnRightFunc);

document.addEventListener("keydown", slideArrows);

function slideArrows(e) {
    if (e.keyCode == 39) {

        btnRightFunc();
    } else if (e.keyCode == 37) {

        btnLeftFunc();
    }

}